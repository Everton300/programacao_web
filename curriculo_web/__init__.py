from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html", title="Página inicial")


@app.route('/curriculo')
def curriculo():
    return render_template("curriculo.html", title="Currículo")